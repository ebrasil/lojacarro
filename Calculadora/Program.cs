﻿using System;

namespace Calculadora
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculadora calculadora = new Calculadora();

            calculadora.Num1 = double.Parse(Console.ReadLine());
            calculadora.Num2 = double.Parse(Console.ReadLine());
            calculadora.Operacao = Console.ReadLine();

            double resultado = calculadora.ExecutarCalculo();


            Console.WriteLine(resultado);
        }
    }
}
