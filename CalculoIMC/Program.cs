﻿using System;

namespace CalculoIMC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Usuario usuario = new Usuario();
            usuario.nome = "Edson";
            usuario.peso = 65;
            usuario.altura = 1.68;

            IMC imc = new IMC();

            double valorImc = imc.CalcularIMC(usuario);
            Console.WriteLine(valorImc);

            string classificacao = imc.ClassificarIMC(valorImc);

            Console.WriteLine(classificacao);
        }
    }
}
