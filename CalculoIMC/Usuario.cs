﻿using System;
using System.Collections.Generic;
using System.Text;


namespace CalculoIMC
{
    class Usuario
    {
        public string nome { get; set; }
        public double peso { get; set; }
        public double altura { get; set; }
        public int idade { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
    }

    class IMC
    {
        public double CalcularIMC(Usuario usuario)
        {
            double imc = usuario.peso / (usuario.altura * usuario.altura);

            return imc;
        }

        public string ClassificarIMC(double imc)
        {
            if(imc < 18.5)
            {
                return "Baixo Peso";
            }else
            {
                if(imc >= 18.5 && imc <= 24.9)
                {
                    return "Peso Normal";
                }else
                {
                    if(imc >= 25 && imc <= 29.9)
                    {
                        return "Pré-obeso";
                    }else
                    {
                        if(imc >= 30 && imc <= 34.9)
                        {
                            return "Obeso";
                        }else
                        {
                            if(imc >= 35 && imc <= 39.9)
                            {
                                return "Obeso II";
                            }else
                            {
                                return "Obeso III";
                            }
                        }
                    }
                }
            }
        }
    }


}
