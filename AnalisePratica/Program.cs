﻿using System;

namespace AnalisePratica
{
    class Program
    {
        static void Main(string[] args)
        {
            MaiorOuMenor maiorOuMenor  = new MaiorOuMenor();

            maiorOuMenor.Num1 = 1;
            maiorOuMenor.Num2 = 2;
            maiorOuMenor.Num3 = 3;

            int maior = maiorOuMenor.Maior();
            Console.WriteLine(maior);

            int menor = maiorOuMenor.Menor();
            Console.WriteLine(menor);
        }
    }
}
