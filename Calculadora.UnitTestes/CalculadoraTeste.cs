using NUnit.Framework;
using Calculadora;

namespace Calculadora.UnitTestes
{
    public class CalculadoraTeste
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForSomar_RetornaSoma()
        {
            //arrange
            var calculadora = new Calculadora();

            calculadora.Num1 = 1;
            calculadora.Num2 = 2;
            calculadora.Operacao = "somar";

            //act
            var resultado = calculadora.ExecutarCalculo();

            //assert
            Assert.That(resultado, Is.EqualTo(3));
        }
        [Test]
        public void ExecutarCalculo_QuandoAcaoForSubtrair_RetornaSubtracao()
        {
            //arrange
            var calculadora = new Calculadora();

            calculadora.Num1 = 1;
            calculadora.Num2 = 2;
            calculadora.Operacao = "subtrair";

            //act
            var resultado = calculadora.ExecutarCalculo();

            //assert
            Assert.That(resultado, Is.EqualTo(-1));
        }
        [Test]
        public void ExecutarCalculo_QuandoAcaoForMultiplicar_RetornaMultiplicacao()
        {
            //arrange
            var calculadora = new Calculadora();

            calculadora.Num1 = 1;
            calculadora.Num2 = 2;
            calculadora.Operacao = "multiplicacao";

            //act
            var resultado = calculadora.ExecutarCalculo();

            //assert
            Assert.That(resultado, Is.EqualTo(2));
        }
        [Test]
        public void ExecutarCalculo_QuandoAcaoForDividirPorZero_RetornaPositiveInfinity()
        {
            //arrange
            var calculadora = new Calculadora();

            calculadora.Num1 = 1;
            calculadora.Num2 = 0;
            calculadora.Operacao = "divisao";

            //act
            var resultado = calculadora.ExecutarCalculo();

            //assert
            Assert.That(resultado, Is.EqualTo(double.PositiveInfinity));
        }
    }
}